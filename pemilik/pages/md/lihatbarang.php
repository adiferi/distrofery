<?php 
	include'proses/koneksi.php';
?>

<h2 align="center">DATA BARANG</h2>
<br>
<table class="table table-hover table-responsive table-striped" border="0">
	<thead>
	<tr>
		<th>No</th>
		<th>Nama Barang</th>
		<th>Kategori</th>
		<th>Deskripsi</th>
		<th>Stok</th>
		<th>Harga</th>
		<th>Gambar</th>
		<th>Berat Barang</th>
	</tr>
</thead>
	
<?php 
	$nobarang=1;
	$tampilbarang = mysqli_query($koneksi,"SELECT * FROM barang");
	while ($barang = mysqli_fetch_array($tampilbarang)) {
		?>
	<tr>
		<td><?php echo $nobarang++ ?></td>
		<td><?php echo $barang['nama_barang']; ?></td>
		<td><?php echo $barang['kategori']; ?></td>
		<td><?php echo $barang['deskripsi']; ?></td>
		<td><?php echo $barang['stok']; ?></td>
		<td><?php echo $barang['harga_barang']; ?></td>
		<td><img src="<?php echo "pages/md/gambarbarang/".$barang['gambar']; ?>" width="200" height="200"></td>
		<td><?php echo $barang['berat_barang']; ?></td>
		<td>
			<a class="btn btn-primary" role="button" href="pages/md/formeditbarang.php?id_barang=<?php echo $barang['id_barang']; ?>">Edit</a>
			<a class="btn btn-danger" role="button" href="pages/md/proses/hapusbarang.php?id_barang=<?php echo $barang['id_barang']; ?>">Hapus</a>
		</td>
	</tr>
	<?php
	}
?>
</table>
</div>