/*
SQLyog Ultimate
MySQL - 10.1.34-MariaDB : Database - jualonline
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id_admin` int(4) NOT NULL,
  `nama_admin` varchar(20) NOT NULL,
  `username_admin` varchar(20) NOT NULL,
  `password_admin` varchar(20) NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `admin` */

LOCK TABLES `admin` WRITE;

UNLOCK TABLES;

/*Table structure for table `barang` */

DROP TABLE IF EXISTS `barang`;

CREATE TABLE `barang` (
  `id_barang` int(10) NOT NULL AUTO_INCREMENT,
  `nama_barang` varchar(35) NOT NULL,
  `kategori` varchar(20) NOT NULL,
  `deskripsi` text NOT NULL,
  `stok` int(10) NOT NULL,
  `harga_barang` int(10) NOT NULL,
  `gambar` blob NOT NULL,
  `berat_barang` int(10) NOT NULL,
  `size` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id_barang`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `barang` */

LOCK TABLES `barang` WRITE;

insert  into `barang`(`id_barang`,`nama_barang`,`kategori`,`deskripsi`,`stok`,`harga_barang`,`gambar`,`berat_barang`,`size`) values (3,'HARD WORK','weqweqwe',' Deskripsi',87,10000,'27345219_008bc561-7ef8-4688-bda9-a3878083072d_2048_1920.jpg',25,'XL'),(4,'dream big','weqweqwe',' 123',111,20000,'27345219_008bc561-7ef8-4688-bda9-a3878083072d_2048_1920.jpg',60,'XL');

UNLOCK TABLES;

/*Table structure for table `detail_kirim` */

DROP TABLE IF EXISTS `detail_kirim`;

CREATE TABLE `detail_kirim` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_transaksi` varchar(20) DEFAULT NULL,
  `tujuan` varchar(50) DEFAULT NULL,
  `kurir` varchar(50) DEFAULT NULL,
  `estimasi` int(11) DEFAULT NULL,
  `biaya_kirim` float DEFAULT NULL,
  `resi` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `detail_kirim` */

LOCK TABLES `detail_kirim` WRITE;

insert  into `detail_kirim`(`id`,`id_transaksi`,`tujuan`,`kurir`,`estimasi`,`biaya_kirim`,`resi`) values (11,'20191307-1562995105','','tiki',3,48000,'234234234'),(12,'20191307-1563027761','','pos',1,30000,'123123'),(13,'20191407-1563096371','','jne',7,46000,NULL),(14,'20191407-1563096450','','tiki',3,9000,NULL),(15,'20191407-1563102485','','tiki',3,72000,'123123123123123123123123'),(16,'20191407-1563103148','','tiki',3,48000,'123123123123123'),(17,'20191407-1563105015','','jne',7,46000,'');

UNLOCK TABLES;

/*Table structure for table `detail_transaksi` */

DROP TABLE IF EXISTS `detail_transaksi`;

CREATE TABLE `detail_transaksi` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_transaksi` varchar(20) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `jumlah_beli` int(11) DEFAULT NULL,
  `harga_barang` float DEFAULT NULL,
  `berat` int(11) DEFAULT NULL,
  `sub_total` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

/*Data for the table `detail_transaksi` */

LOCK TABLES `detail_transaksi` WRITE;

insert  into `detail_transaksi`(`id`,`id_transaksi`,`id_barang`,`jumlah_beli`,`harga_barang`,`berat`,`sub_total`) values (22,'20191307-1562995105',2,4,2000,20,8000),(23,'20191307-1563027761',2,1,2000,5,2000),(24,'20191407-1563096371',3,4,10000,100,40000),(25,'20191407-1563096450',3,2,10000,50,20000),(26,'20191407-1563102363',3,2,10000,50,20000),(27,'20191407-1563102363',3,1,10000,25,10000),(29,'20191407-1563102393',3,1,10000,25,10000),(30,'20191407-1563102485',3,1,10000,25,10000),(31,'20191407-1563103148',3,1,10000,25,10000),(32,'20191407-1563105015',3,1,10000,25,10000);

UNLOCK TABLES;

/*Table structure for table `keranjang` */

DROP TABLE IF EXISTS `keranjang`;

CREATE TABLE `keranjang` (
  `id_keranjang` int(10) NOT NULL AUTO_INCREMENT,
  `id_pelanggan` int(10) NOT NULL,
  `id_barang` int(10) NOT NULL,
  `id_pesan` int(10) NOT NULL,
  `jumlah_barang` int(10) NOT NULL,
  `harga` int(10) NOT NULL,
  `total_berat` int(10) NOT NULL,
  `sub_harga` int(10) NOT NULL,
  PRIMARY KEY (`id_keranjang`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `keranjang` */

LOCK TABLES `keranjang` WRITE;

UNLOCK TABLES;

/*Table structure for table `konfirmasi_pembayaran` */

DROP TABLE IF EXISTS `konfirmasi_pembayaran`;

CREATE TABLE `konfirmasi_pembayaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_transaksi` varchar(20) DEFAULT NULL,
  `foto` varchar(150) DEFAULT NULL,
  `waktu_pembayaran` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `konfirmasi_pembayaran` */

LOCK TABLES `konfirmasi_pembayaran` WRITE;

insert  into `konfirmasi_pembayaran`(`id`,`id_transaksi`,`foto`,`waktu_pembayaran`) values (2,'20191307-1562995105','pundong.png','2019-07-14 04:56:10'),(3,'20191307-1563027761','Capture.PNG','2019-07-14 04:56:14'),(4,'','','0000-00-00 00:00:00'),(5,'','','0000-00-00 00:00:00'),(6,'','','0000-00-00 00:00:00'),(7,'20191407-1563103148','MAHAKARTA.jpg','2019-07-15 01:22:53'),(8,'20191407-1563102393','MAHAKARTA.jpg','2019-07-15 01:23:10'),(9,'20191407-1563102485','MAHAKARTA.jpg','2019-07-15 01:23:21'),(10,'20191407-1563105015','MAHAKARTA.jpg','2019-07-15 01:50:45');

UNLOCK TABLES;

/*Table structure for table `pelanggan` */

DROP TABLE IF EXISTS `pelanggan`;

CREATE TABLE `pelanggan` (
  `id_pelanggan` int(10) NOT NULL AUTO_INCREMENT,
  `nama_pelanggan` varchar(30) NOT NULL,
  `username_pelanggan` varchar(20) NOT NULL,
  `password_pelanggan` varchar(20) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `alamat` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `foto` blob NOT NULL,
  PRIMARY KEY (`username_pelanggan`),
  KEY `id_pelanggan` (`id_pelanggan`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `pelanggan` */

LOCK TABLES `pelanggan` WRITE;

insert  into `pelanggan`(`id_pelanggan`,`nama_pelanggan`,`username_pelanggan`,`password_pelanggan`,`no_telp`,`alamat`,`email`,`foto`) values (4,'123','123','123','081542933406','123','adiferiismail@gmail.com','MAHAKARTA.jpg');

UNLOCK TABLES;

/*Table structure for table `pembayaran` */

DROP TABLE IF EXISTS `pembayaran`;

CREATE TABLE `pembayaran` (
  `id_pembayaran` int(10) NOT NULL,
  `id_pelanggan` int(10) NOT NULL,
  `alamat_pengiriman` text NOT NULL,
  `ekspedisi` varchar(20) NOT NULL,
  `harga_ongkir` int(10) NOT NULL,
  `total_bayar` int(10) NOT NULL,
  `tanggal_pembayaran` date NOT NULL,
  `foto_pembayaran` blob NOT NULL,
  PRIMARY KEY (`id_pembayaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pembayaran` */

LOCK TABLES `pembayaran` WRITE;

UNLOCK TABLES;

/*Table structure for table `pesanmanual` */

DROP TABLE IF EXISTS `pesanmanual`;

CREATE TABLE `pesanmanual` (
  `id_pesan` int(10) NOT NULL,
  `id_pelanggan` int(10) NOT NULL,
  `nama_pesan` varchar(50) NOT NULL,
  `deskripsi_pesan` text NOT NULL,
  `jumlah_pesan` int(10) NOT NULL,
  `berat_pesan` int(10) NOT NULL,
  `gambar_pesan` blob NOT NULL,
  PRIMARY KEY (`id_pesan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pesanmanual` */

LOCK TABLES `pesanmanual` WRITE;

UNLOCK TABLES;

/*Table structure for table `statuspembayaran` */

DROP TABLE IF EXISTS `statuspembayaran`;

CREATE TABLE `statuspembayaran` (
  `id_status` int(10) NOT NULL,
  `id_pelanggan` int(10) NOT NULL,
  `id_pembayaran` int(10) NOT NULL,
  `status_pesan` varchar(200) NOT NULL,
  `resi` varchar(100) NOT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `statuspembayaran` */

LOCK TABLES `statuspembayaran` WRITE;

UNLOCK TABLES;

/*Table structure for table `transaksi` */

DROP TABLE IF EXISTS `transaksi`;

CREATE TABLE `transaksi` (
  `id_transaksi` varchar(20) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_user` varchar(10) DEFAULT NULL,
  `total_transaksi` float DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_transaksi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `transaksi` */

LOCK TABLES `transaksi` WRITE;

insert  into `transaksi`(`id_transaksi`,`tanggal`,`id_user`,`total_transaksi`,`status`) values ('20191307-1562995105','2019-07-15 00:29:46','4',56000,'terkirim'),('20191307-1563027761','2019-07-15 00:45:30','4',32000,'terkirim'),('20191407-1563096371','2019-07-15 00:29:36','4',86000,'dibayar'),('20191407-1563096450','2019-07-15 00:29:18','4',29000,'terbayar'),('20191407-1563102363','2019-07-15 01:15:50','4',40000,'dibayar'),('20191407-1563102393','2019-07-15 01:24:03','4',58000,'terkirim'),('20191407-1563102485','2019-07-15 01:24:12','4',82000,'terkirim'),('20191407-1563103148','2019-07-15 01:23:55','4',58000,'terkirim'),('20191407-1563105015','2019-07-15 01:50:45','4',56000,'dibayar');

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
