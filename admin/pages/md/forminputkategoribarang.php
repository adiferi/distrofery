<?php 
include ('koneksi.php');
if (!empty($_POST['simpan'])) {
	$simpanbarang = mysqli_query($koneksi,"INSERT into kategori_barang value ('','".$_POST['nama_type']."')");
	if ($simpanbarang==1) {
		echo '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                Success simpan
              </div>';
	}else{
		echo '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                Success Gagal
              </div>';
	}
}
?>

<div class="col-md-3">
	<div class="box">
		<div class="box-header">
			<h4>INPUT KATEGORI BARANG</h4>
		</div>
		<div class="box-body">
			<div align="left">
				<form action="#" method="POST" >
					<div class="form-group">
						<label>Nama Kategori</label>
						<input type="text" name="nama_type" class="form-control">
						<input type="hidden" name="id" class="form-control">
					</div>
					<button type="submit" class="btn btn-primary" value="simpan" name="simpan">Simpan</button>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="col-md-9">
	<div class="box">
		<div class="box-header">
		</div>
		<div class="box-body">
			<table class="table table-striped table-bordered">
				<thead align="center">
					<th>no</th>
					<th>nama</th>
					<th>aksi</th>
				</thead>
				<tbody>
					<?php 
					include ('koneksi.php');
					$nobarang = 1;
					$tampilbarang = mysqli_query($koneksi,"SELECT * FROM kategori_barang");
					while ($barang = mysqli_fetch_array($tampilbarang)) {
						?>
						<tr>
							<td><?php echo $nobarang++ ?></td>
							<td><?php echo $barang['nama_tipe']; ?></td>
							<td>
								<a class="btn btn-primary" role="button" href="pages/md/formeditbarang.php?id_barang=<?php echo $barang['id_barang']; ?>">Edit</a>
								<a class="btn btn-danger" role="button" href="pages/md/proses/hapuskategoribarang.php?id_kategori=<?php echo $barang['id_kategori']; ?>">Hapus</a>
							</td>
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>

</div>


<script>
function hanyaAngka(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))

		return false;
	return true;
}
</script>