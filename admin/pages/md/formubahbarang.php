<?php 
$barang = $_GET['id_barang'];
?>

<div class="box">
	<div class="box-body">
		<div align="left">
			<h2 align="center">Ubah Data Barang</h2>
			<form action="pages/md/proses/ubahbarang.php" method="POST" enctype="multipart/form-data" >
				<?php 
				$db = mysqli_query($koneksi,"SELECT * FROM barang where id_barang='".$barang."' ");
				while ($dbl = mysqli_fetch_array($db)) { ?>
				<table border="0" class="table" >
					<br>
					<tr>
						<td class="col-xs-2">Nama Barang</td>
						<td>
							<input type="hidden" name="id_barang" value="<?php echo $dbl['id_barang']; ?>">
							<input type="text" name="nama_barang" class="form-control" value="<?php echo $dbl['nama_barang']; ?>">
						</td>
					</tr>
					<tr>
						<td class="col-xs-2">Kategori</td>
						<td>
							<select name="kategori" class="form-control" >
								<?php 
								$dbss = mysqli_query($koneksi,"SELECT * FROM kategori_barang");
								while ($a = mysqli_fetch_array($dbss)) { ?>
								<option value="<?php echo $a['nama_tipe']; ?>" 
									<?php 
									if($a['nama_tipe']==$dbl['kategori']){
										echo "selected='selected'";
									};
									?>
									>
									<?php echo $a['nama_tipe']; ?></option>
									<?php } ?>	
								</select>
							</td>
						</tr>
						<tr>
							<tr>
								<td class="col-xs-2">Deskripsi</td>
								<td>
									<textarea name="deskripsi" class="form-control"><?php echo $dbl['deskripsi']; ?></textarea>
								</td>
							</tr>

							<tr>
								<td class="col-xs-2">Harga</td>
								<td>
									<input type="text" name="harga" class="form-control" onkeypress="return hanyaAngka(event)" value="<?php echo $dbl['harga']; ?>">
								</td>
							</tr>
							<tr>
								<td class="col-xs-2">Berat Barang</td>
								<td>
									<input type="text" name="berat_barang" class="form-control" onkeypress="return hanyaAngka(event)" value="<?php echo $dbl['berat_barang']; ?>">
								</td>
							</tr>

							<tr>
								<!-- <td class="col-xs-2">Upload Gambar</td> -->
								<td>
									<!-- <input type="file" name="gambar" value=""> -->
								</td>
							</tr>
						</table>
						<div class="box">
							<div class="box-header">
								<h4>Ubah Stok Ada</h4>
							</div>
							<div class="box-body">
								<table class="table table-striped table-bordered" id="dynamic_field">
									<thead>
										<th>Ukuran</th>
										<th>Stok (Pcs)</th>
									</thead>
									<body>
										<?php 
										$dbss = mysqli_query($koneksi,"SELECT * FROM detail_barang where id_barang='".$_GET['id_barang']."' ");
										while ($dblss = mysqli_fetch_array($dbss)) { ?>
										<tr>
											<td>
												<input type="hidden" name="id[]" value="<?php echo $dblss['id_detail_barang'] ?>">
												<label><?php echo $dblss['size']; ?></label>
											</td>
											<td>
												<input type="number" class="form-control" name="stok[]" value="<?php echo $dblss['stok'] ?>">
											</td>
										</tr>
										<?php } ?>
									</body>
								</table>
								<?php } ?>
							</div>
						</div>
					</div>
					<div align="right">
						<input type="submit" value="Simpan" name=inputbarang class="btn btn-primary">
					</div>
				</form>
			</div>
		</div>

		<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script>
		function hanyaAngka(evt) {
			var charCode = (evt.which) ? evt.which : event.keyCode
			if (charCode > 31 && (charCode < 48 || charCode > 57))

				return false;
			return true;
		}

		$(document).ready(function(){
			var i =0;
			$('#add').click(function(){
				var html = '<tr><td><select class="form-control" name="size[]"><option value="">-PILIH UKURAN-</option><option value="S">S</option>'+
				'<option value="M">M</option>'+
				'<option value="L">L</option>'+
				'<option value="XL">XL</option>'+
				'<option value="XXL">XXL</option>'+
				'<option value="3XL">3XL</option>'+
				'</select></td><td><input type="number" class="form-control" name="stok[]">'+
				'</td><td>'+
				'</td></tr>';
				$('#dynamic_field').append(html);
			});
		})
	</script>